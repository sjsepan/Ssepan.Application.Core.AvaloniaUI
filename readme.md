# readme.md - README for Ssepan.Application.Core.Avalonia

## TODO

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

## Update Avalonia

dotnet add package Avalonia --version 11.0.6
dotnet add package Avalonia.Desktop --version 11.0.6
dotnet add package Avalonia.Diagnostics --version 11.0.6

Avalonia.Visual (AKA NP.Avalonia.Visuals) is deprecated and not compatible with Avalonia 11; use NP.Ava.Visuals instead:
<https://www.nuget.org/packages/NP.Avalonia.Visuals>
dotnet add package NP.Ava.Visuals --version 1.0.4

### History

6.2:
~test app w/ add'l themes: Simple, Classic (Classic.Avalonia.Theme by @bandysc); set current to Classic

6.1:
~replace obsolete dialog calls with calls to IStorageProvider dialogs

6.0:
~update to net8.0
~refactor to newer C# features
~refactor to existing language types
~perform format linting
~redo command-line args and how passed filename overrides config file settings, including addition of a format arg.

5.3:
~switch from MessageBox.Avalonia to custom version derived from MessageBox by kekekeks

5.2:
~move usings
~implement dlg filters
~trap new filenames
~use dlg info BoolResult
~use ACTION_ consts in status
~set dlg titles to action not appname
~move progress/status from view to VM
~add addl std menu actions to vm

5.1:
~Clone from Ssepan.Application.Core.* lib

## Contact

Steve Sepan
<sjsepan@yahoo.com>
12/11/2024
