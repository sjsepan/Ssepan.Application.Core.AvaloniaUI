// derived from MessageBox by kekekeks:
//Author: @kekekeks (https://stackoverflow.com/users/2231814/kekekeks)
//Source: https://stackoverflow.com/questions/55706291/how-to-show-a-message-box-in-avaloniaui-beta

using System;
using System.Threading.Tasks;
using Ssepan.Utility.Core;
using System.Reflection;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;

namespace Ssepan.Application.Core.AvaloniaUI
{
    public partial class MessageBox :
        Window
    {
        public enum MessageBoxButtons
        {
            Ok,
            OkCancel,
            YesNo,
            YesNoCancel
        }

        public enum MessageBoxResult
        {
            None,
            Ok,
            Cancel,
            Yes,
            No
        }

        public enum MessageType
        {
            Info,
            Question,
            Warning,
            Error
        }

        public MessageBox()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public static Task<MessageBoxResult> Show
        (
            Window parent,
            string text,
            string title,
            MessageBoxButtons buttons
        )
        {
			TaskCompletionSource<MessageBoxResult> taskCompletionSource = null;
			MessageBox messageBox = null;
			StackPanel buttonPanel = null;
            MessageBoxResult messageBoxResult = MessageBoxResult.None;//.Ok;

			try
			{
				messageBox = new MessageBox()
				{
					Title = title
				};
				messageBox.FindControl<TextBlock>("Text").Text = text;
				buttonPanel = messageBox.FindControl<StackPanel>("Buttons");

                //=================
                void AddButton(string caption, MessageBoxResult resultIfSelected, bool isDefaultValue = false)
				{
					Button btn = new() { Content = caption };
					btn.Click += (object sender, RoutedEventArgs e) =>
					{
						messageBoxResult = resultIfSelected;
						messageBox.Close();
					};

					buttonPanel.Children.Add(btn);

					if (isDefaultValue)
					{
						messageBoxResult = resultIfSelected;
					}
				}
                //=================

				if (buttons == MessageBoxButtons.Ok || buttons == MessageBoxButtons.OkCancel)
					AddButton("Ok", MessageBoxResult.Ok, true);
				if (buttons == MessageBoxButtons.YesNo || buttons == MessageBoxButtons.YesNoCancel)
				{
					AddButton("Yes", MessageBoxResult.Yes);
					AddButton("No", MessageBoxResult.No, true);
				}

				if (buttons == MessageBoxButtons.OkCancel || buttons == MessageBoxButtons.YesNoCancel)
				{
					AddButton("Cancel", MessageBoxResult.Cancel, true);
				}

				taskCompletionSource = new TaskCompletionSource<MessageBoxResult>();
				messageBox.Closed += delegate { taskCompletionSource.TrySetResult(messageBoxResult); };

                if (parent != null)
				{
					messageBox.ShowDialog(parent);
				}
				else
				{
					messageBox.Show();
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
			}

            return taskCompletionSource.Task;
		}

        public async static Task<MessageBoxResult> ShowAsync
        (
            Window parent,
            string text,
            string title,
            MessageBoxButtons buttons
        )
        {
			MessageBox messageBox = null;
            StackPanel buttonPanel = null;
            MessageBoxResult messageBoxResult = MessageBoxResult.None;//.Ok;

			try
            {
                messageBox = new MessageBox()
                {
                    Title = title
                };
                messageBox.FindControl<TextBlock>("Text").Text = text;
                buttonPanel = messageBox.FindControl<StackPanel>("Buttons");

                //=================
                void AddButton(string caption, MessageBoxResult resultIfSelected, bool isDefaultValue = false)
                {
                    Button button = new() { Content = caption};
                    button.Click += (object sender, RoutedEventArgs e) =>
                    {
                        messageBoxResult = resultIfSelected;
                        messageBox.Close();
                    };

                    buttonPanel.Children.Add(button);

                    if (isDefaultValue)
                    {
                        messageBoxResult = resultIfSelected;
                    }
                }
                //=================

                if (buttons == MessageBoxButtons.Ok || buttons == MessageBoxButtons.OkCancel)
                {
                    AddButton("Ok", MessageBoxResult.Ok, true);
                }
                if (buttons == MessageBoxButtons.YesNo || buttons == MessageBoxButtons.YesNoCancel)
                {
                    AddButton("Yes", MessageBoxResult.Yes);
                    AddButton("No", MessageBoxResult.No, true);
                }

				if (buttons == MessageBoxButtons.OkCancel || buttons == MessageBoxButtons.YesNoCancel)
				{
					AddButton("Cancel", MessageBoxResult.Cancel, true);
				}

				if (parent != null)
                {
                    await messageBox.ShowDialog(parent);
                }
                else
                {
                    messageBox.Show();
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return messageBoxResult;
        }
    }
}