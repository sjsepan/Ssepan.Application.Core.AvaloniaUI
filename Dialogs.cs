﻿using System;
using System.Collections.Generic;
using System.IO;
using Ssepan.Io.Core;
using Ssepan.Utility.Core;
//using Microsoft.Data.ConnectionUI;//cannot use until Microsoft.VisualStudio.Data.dll is made re-distributable by Microsoft
using System.Reflection;
using System.Threading.Tasks;
// using Avalonia_ = Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Media.Imaging;
using Avalonia;
using Avalonia.Platform.Storage;
using System.Linq;
using Avalonia.Data.Core;
//using Bitmap = Avalonia.Media.Imaging.Bitmap;

namespace Ssepan.Application.Core.AvaloniaUI
{
	/// <summary>
	/// Note: Several static methods are re-implemented as async methods
	/// due to the fact that MessageBox.Avalonia's Show[...] methods force async/await.
	/// </summary>
	public static class Dialogs
    {
        #region Declarations
        public const string FilterSeparator = "|";
        public const string FilterDescription = "{0} Files(s)";
        public const string FilterFormat = "{0} (*.{1})|*.{1}";
        #endregion Declarations

        #region Methods
        /// <summary>
        /// Get path to save data.
        /// Static. Calls Avalonia.SaveFileDialog.
        /// </summary>
        /// <param name="fileDialogInfo">FileDialogInfo<Window, MessageBox.MessageBoxResult></param>
        /// <returns>fileDialogInfo</returns>
        public async static Task<FileDialogInfo<Window, MessageBox.MessageBoxResult>> GetPathForSave
        (
            FileDialogInfo<Window, MessageBox.MessageBoxResult> fileDialogInfo,
            Window view
        )
        {
            bool returnValue = default;
			try
			{
                if
                (
                    fileDialogInfo.Filename.EndsWith(fileDialogInfo.NewFilename)
                    ||
                    fileDialogInfo.ForceDialog
                )
                {
                    // these simple calls are now 'obsolete', as of v11.0.x
                    // SaveFileDialog fileDialog = new()
                    // {
                    //     Title = fileDialogInfo.ForceDialog ? "Save As..." : "Save...",
                    //     Directory =

                    //         fileDialogInfo.InitialDirectory == default
                    //         ?
                    //         fileDialogInfo.CustomInitialDirectory
                    //         :
                    //         Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
                    //     ,
                    //     InitialFileName = fileDialogInfo.Filename
                    // };

                    // IStorageProvider is the new direction indicated by AvaloniaUI,
                    TopLevel topLevel = TopLevel.GetTopLevel(view);

                    FilePickerSaveOptions options = new()
                    {
                        Title = fileDialogInfo.ForceDialog ? "Save file As..." : "Save file..."
                    };

                    List<FilePickerFileType> filters = new /*FilePickerFileType*/[] { FilePickerFileTypes.TextPlain, FilePickerFileTypes.All }.ToList();
                    foreach (string filter in fileDialogInfo.Filters.Split(FileDialogInfo<Window, MessageBox.MessageBoxResult>.FILTER_SEPARATOR))
                    {
						string[] nameAndPattern = filter.Split(FileDialogInfo<Window, MessageBox.MessageBoxResult>.FILTER_ITEM_SEPARATOR);
                        // FileDialogFilter fileDialogFilter = new()
                        // {
                        // 	Name = nameAndPattern[0]
                        // };
                        // fileDialogFilter.Extensions.Add(nameAndPattern[1]);
                        // fileDialog.Filters.Add(fileDialogFilter);//?
                        filters.Add(new /*FilePickerFileType*/(nameAndPattern[0]) { Patterns = new /*IReadOnlyList<String>*/[] {nameAndPattern[1] } });
                    }
                    //options.FileTypeFilter = filters;

					// string fileResult = await fileDialog.ShowAsync(fileDialogInfo.Parent);
                    IStorageFile fileResult = await topLevel.StorageProvider.SaveFilePickerAsync(options);

					if (!string.IsNullOrWhiteSpace(fileResult.Path.AbsolutePath))
                    {
                        if (string.Equals(Path.GetFileName(fileResult.Name), fileDialogInfo.NewFilename, StringComparison.CurrentCultureIgnoreCase))
                        {
							//user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
							string messageTemp = string.Format("The name \"{0}.{1}\" is not allowed; please choose another. Settings not saved.", fileDialogInfo.NewFilename.ToLower(), fileDialogInfo.Extension.ToLower());
							MessageDialogInfo<Window, MessageBox.MessageBoxResult, object, MessageBox.MessageType, MessageBox.MessageBoxButtons> messageDialogInfo =
					new (
						fileDialogInfo.Parent,
						fileDialogInfo.Modal,
						fileDialogInfo.Title,
						null,
						MessageBox.MessageType.Info,
						MessageBox.MessageBoxButtons.Ok,
						messageTemp,
						MessageBox.MessageBoxResult.None
					);

							// Dialogs d = new Dialogs();
							// ShowMessageDialog
							// (
							//     ref messageDialogInfo,
							//     ref errorMessage
							// );
							messageDialogInfo = await ShowMessageDialog(messageDialogInfo);
                            // d = null;

                            //Forced cancel
                            fileDialogInfo.Response = MessageBox.MessageBoxResult.Cancel;
                        }
                        else
                        {
                            //set new filename
                            fileDialogInfo.Filename = fileResult.Path.AbsolutePath;
                            fileDialogInfo.Filenames = new string[] { fileResult.Path.AbsolutePath};
                            fileDialogInfo.Response = MessageBox.MessageBoxResult.Ok;
                            returnValue = true;
                        }
                    }
                    else
                    {
                        fileDialogInfo.Response = MessageBox.MessageBoxResult.Cancel;
                        returnValue = true;
                    }
                }
                else
                {
                    fileDialogInfo.Response = MessageBox.MessageBoxResult.Ok;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                fileDialogInfo.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            fileDialogInfo.BoolResult = returnValue;
            return fileDialogInfo;
        }

        /// <summary>
        /// Get path to load data.
        /// Static. Calls Avalonia.OpenFileDialog.
        /// </summary>
        /// <param name="fileDialogInfo">FileDialogInfo<Window, MessageBox.MessageBoxResult></param>
        /// <returns>Task<FileDialogInfo<Window, MessageBox.MessageBoxResult>></returns>
        public async static Task<FileDialogInfo<Window, MessageBox.MessageBoxResult>> GetPathForLoad
        (
            FileDialogInfo<Window, MessageBox.MessageBoxResult> fileDialogInfo,
            Window view
        )
        {
            bool returnValue = default;
			try
			{
                if (fileDialogInfo.ForceNew)
                {
                    fileDialogInfo.Filename = fileDialogInfo.NewFilename;

                    returnValue = true;
                }
                else
                {

                    // these simple calls are now 'obsolete', as of v11.0.x
					// OpenFileDialog fileDialog =
                    // new()
                    // {
                    //     Title = fileDialogInfo.Title,
                    //     Directory =

                    //         fileDialogInfo.InitialDirectory == default
                    //         ?
                    //         fileDialogInfo.CustomInitialDirectory
                    //         :
                    //         Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
                    //     ,
                    //     InitialFileName = fileDialogInfo.Filename,
                    //     AllowMultiple = fileDialogInfo.Multiselect
                    // };

                    // IStorageProvider is the new direction indicated by AvaloniaUI,
                    TopLevel topLevel = TopLevel.GetTopLevel(view);
                    FilePickerOpenOptions options = new()
                    {
                        Title =  fileDialogInfo.Title,//"Open a file"
                        //Environment.GetFolderPath(Environment.SpecialFolder.Personal)
                        // FileTypeFilter.?,
                        // SuggestedStartLocation = fileDialogInfo.InitialDirectory == default
                        //         ?
                        //         fileDialogInfo.CustomInitialDirectory
                        //         :
                        //         Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
                        //,
                        SuggestedFileName = fileDialogInfo.Filename,
                        AllowMultiple = fileDialogInfo.Multiselect
                    };

                    List<FilePickerFileType> filters = new /*FilePickerFileType*/[] { FilePickerFileTypes.TextPlain, FilePickerFileTypes.All }.ToList();
                    foreach (string filter in fileDialogInfo.Filters.Split(FileDialogInfo<Window, MessageBox.MessageBoxResult>.FILTER_SEPARATOR))
                    {
						string[] nameAndPattern = filter.Split(FileDialogInfo<Window, MessageBox.MessageBoxResult>.FILTER_ITEM_SEPARATOR);
                        // FileDialogFilter fileDialogFilter = new()
                        // {
                        // 	Name = nameAndPattern[0]
                        // };
                        // fileDialogFilter.Extensions.Add(nameAndPattern[1]);
                        // fileDialog.Filters.Add(fileDialogFilter);//?
                        filters.Add(new /*FilePickerFileType*/(nameAndPattern[0]) { Patterns = new /*IReadOnlyList<String>*/[] {nameAndPattern[1] } });
                    }
                    options.FileTypeFilter = filters;

					// string[] fileResults = await fileDialog.ShowAsync(fileDialogInfo.Parent);
                    IReadOnlyList<IStorageFile> fileResults = await topLevel.StorageProvider.OpenFilePickerAsync(options);

					if (fileResults != null)
                    {
                        if (fileResults.Count>0)
                        {
                            if (!string.IsNullOrWhiteSpace(fileResults[0].Path.AbsolutePath))
                            {
                                fileDialogInfo.Filename = fileResults[0].Path.AbsolutePath;
                                fileDialogInfo.Filenames = fileResults.Select(x => x.Path.AbsolutePath).ToArray();
                                fileDialogInfo.Response = MessageBox.MessageBoxResult.Ok;
                                returnValue = true;
                            }
                        }
                        else
                        {
                            fileDialogInfo.Response = MessageBox.MessageBoxResult.Cancel;
                            returnValue = true;
                        }
                    }
                    else
                    {
                        fileDialogInfo.Response = MessageBox.MessageBoxResult.Cancel;
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                fileDialogInfo.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            fileDialogInfo.BoolResult = returnValue;
            return fileDialogInfo;
        }

        /// <summary>
        /// Select a folder path.
        /// Static. Calls Avalonia.OpenFolderDialog.
        /// </summary>
        /// <param name="fileDialogInfo">returns path in Filename property if fileDialogInfo</param>
        /// <returns>fileDialogInfo</returns>
        public async static Task<FileDialogInfo<Window, /*string*/MessageBox.MessageBoxResult>> GetFolderPath
        (
            FileDialogInfo<Window, /*string*/MessageBox.MessageBoxResult> fileDialogInfo,
            Window view
        )
        {
            bool returnValue = default;
			try
			{
                // these simple calls are now 'obsolete', as of v11.0.x
				// OpenFolderDialog folderDialog =
                // new()
                // {
                //     Title = fileDialogInfo.Title,
                //     Directory = string.IsNullOrEmpty(fileDialogInfo.Filename) ? Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator() : fileDialogInfo.Filename
                // };

				// string folderResult = await folderDialog.ShowAsync(fileDialogInfo.Parent);

                // IStorageProvider is the new direction indicated by AvaloniaUI,
                TopLevel topLevel = TopLevel.GetTopLevel(view);
                FolderPickerOpenOptions options = new()
                {
                    Title = "Open folder..."
                };
                IReadOnlyList<IStorageFolder> folderResults = await topLevel.StorageProvider.OpenFolderPickerAsync(options);

				if (folderResults != null)
                {
                    if (folderResults.Count>0)
                    {
                        if (!string.IsNullOrWhiteSpace(folderResults[0].Path.AbsolutePath))
                        {
                            fileDialogInfo.Filename = folderResults[0].Path.AbsolutePath;
                            fileDialogInfo.Response = MessageBox.MessageBoxResult.Ok;
                            returnValue = true;
                        }
                        else
                        {
                            fileDialogInfo.Response = MessageBox.MessageBoxResult.Cancel;
                            returnValue = true;
                        }
                    }
                    else
                    {
                        fileDialogInfo.Response = MessageBox.MessageBoxResult.Cancel;
                        returnValue = true;
                    }
                }
                else
                {
                    fileDialogInfo.Response = MessageBox.MessageBoxResult.Cancel;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                fileDialogInfo.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            fileDialogInfo.BoolResult = returnValue;
            return fileDialogInfo;
        }

        /// <summary>
        /// Shows an About dialog.
        /// non-Static. Calls MessageBox.Avalonia GetMessageBoxCustomWindow.
        /// </summary>
        /// <param name="aboutDialogInfo">AboutDialogInfo<Window, MessageBox.MessageBoxResult, Bitmap></param>
        /// <returns>Task<AboutDialogInfo<Window, MessageBox.MessageBoxResult, Bitmap>></returns>
        public async static Task<AboutDialogInfo<Window, MessageBox.MessageBoxResult, Bitmap>> ShowAboutDialog
        (
            AboutDialogInfo<Window, MessageBox.MessageBoxResult, Bitmap> aboutDialogInfo
        )
        {
			bool returnValue = default;

            try
            {
				string message =
                    aboutDialogInfo.Title + "\n" +
                    "ProgramName: " + aboutDialogInfo.ProgramName + "\n" +
                    "Version: : " + aboutDialogInfo.Version + "\n" +
                    "Copyright: " + aboutDialogInfo.Copyright + "\n" +
                    "Comments: : " + aboutDialogInfo.Comments + "\n" +
                    "Website: : " + aboutDialogInfo.Website;

				// Icon = aboutDialogInfo.Logo//new Bitmap("Resources/App.png")

				MessageBox.MessageBoxResult messageBoxResult = await MessageBox.Show(aboutDialogInfo.Parent, message, aboutDialogInfo.Title, MessageBox.MessageBoxButtons.Ok);

				if (messageBoxResult != MessageBox.MessageBoxResult.None)
                {
                    // Console.WriteLine("ShowAboutDialog:messageBoxResult"+messageBoxResult.ToString());
                    returnValue = true;
                    aboutDialogInfo.Response = MessageBox.MessageBoxResult.Ok;//force response
                }
                else
                {
                    throw new Exception("HelpAboutAction:messageBoxResult:"+messageBoxResult.ToString());
                }
            }
            catch (Exception ex)
            {
                aboutDialogInfo.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            aboutDialogInfo.BoolResult = returnValue;
            return aboutDialogInfo;
        }

        /// <summary>
        /// Get a printer.
        /// Static.
        /// </summary>
        /// <param name="printerDialogInfo">ref PrinterDialogInfo<Window, MessageBox.MessageBoxResult, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns></returns>
        public static bool GetPrinter
        (
            ref PrinterDialogInfo<Window, MessageBox.MessageBoxResult, string> printerDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            // ? printerDialog = null;
            // string response = null;

            try
            {
                // printerDialog =
                //     new ?
                //     (
                //         printerDialogInfo.Title,
                //         printerDialogInfo.Parent
                //     );
                // printerDialog.Modal = printerDialogInfo.Modal;

                // response = (ResponseType)printerDialog.Run();
                // if (response != ResponseType.None)
                // {
                //     if (response == ResponseType.Ok)
                //     {
                //         printerDialogInfo.Printer = printerDialog.SelectedPrinter;
                //         printerDialogInfo.Name = printerDialog.SelectedPrinter.Name;
                //     }

                //     printerDialogInfo.Response = response;
                    returnValue = true;
                // }
                throw new NotImplementedException("GetPrinter");
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            printerDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Shows a message dialog.
        /// non-Static. Calls MessageBox.Avalonia GetMessageBoxStandardWindow.
        /// </summary>
        /// <param name="messageDialogInfo">MessageDialogInfo<Window, MessageBox.MessageBoxResult, object, MessageBox.MessageType, MessageBox.MessageBoxButtons></param>
        /// <returns>Task<MessageDialogInfo<Window, MessageBox.MessageBoxResult, object, MessageBox.MessageType, MessageBox.MessageBoxButtons>></returns>
        public async static Task<MessageDialogInfo<Window, MessageBox.MessageBoxResult, object, MessageBox.MessageType, MessageBox.MessageBoxButtons>> ShowMessageDialog
        (
            MessageDialogInfo<Window, MessageBox.MessageBoxResult, object, MessageBox.MessageType, MessageBox.MessageBoxButtons> messageDialogInfo
        )
        {
			bool returnValue = default;

			try
            {
				MessageBox.MessageBoxResult messageBoxResult;
				// Icon = messageDialogInfo.Logo//new Bitmap("Resources/App.png")

				if (messageDialogInfo.Parent == null)
				{
					//do the same
					messageBoxResult = await MessageBox.Show(messageDialogInfo.Parent, messageDialogInfo.Message, messageDialogInfo.Title, messageDialogInfo.ButtonsType);
				}
				else
				{
					messageBoxResult = await MessageBox.Show(messageDialogInfo.Parent, messageDialogInfo.Message, messageDialogInfo.Title, messageDialogInfo.ButtonsType);
				}

				//return complex responses in dialogInfo
				if (messageBoxResult != MessageBox.MessageBoxResult.None)
                {
                    returnValue = true;
                }
                messageDialogInfo.Response = messageBoxResult;
            }
            catch (Exception ex)
            {
                messageDialogInfo.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            messageDialogInfo.BoolResult = returnValue;
            return messageDialogInfo;
        }

        /// <summary>
        /// Get a color.
        /// Static. Calls Avalonia.ColorChooserDialog.
        /// </summary>
        /// <param name="colorDialogInfo">ref ColorDialogInfo<Window, string, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetColor
        (
            ref ColorDialogInfo<Window, string, string> colorDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            // Avalonia.ColorChooserDialog colorChooserDialog = null;
            // string response = null;
            // string localRGBA;

            try
            {
                // colorDialogInfo.Color = null;
                // colorChooserDialog =
                //     new ColorChooserDialog
                //     (
                //         colorDialogInfo.Title,
                //         colorDialogInfo.Parent
                //     );
                // colorChooserDialog.Modal = colorDialogInfo.Modal;

                // response = (string)colorChooserDialog.Run();
                // if (!string.IsNullOrWhiteSpace(response))
                // {
                //     if (response == "ResponseType."Ok")
                //     {
                //         localRGBA = colorDialogInfo.Color;
                //         localRGBA.Red = colorChooserDialog.Rgba.Red;
                //         localRGBA.Green = colorChooserDialog.Rgba.Green;
                //         localRGBA.Blue = colorChooserDialog.Rgba.Blue;
                //         colorDialogInfo.Color = localRGBA;
                //         // Console.WriteLine
                //         // (
                //         //     string.Format
                //         //     (
                //         //         "Red:{0},Green:{1},Blue:{2}",
                //         //         colorDialogInfo.Color.Red.ToString(),
                //         //         colorDialogInfo.Color.Green.ToString(),
                //         //         colorDialogInfo.Color.Blue.ToString()
                //         //     )
                //         // );
                //         // Console.WriteLine
                //         // (
                //         //     string.Format
                //         //     (
                //         //         "Red:{0},Green:{1},Blue:{2}",
                //         //         colorChooserDialog.Rgba.Red.ToString(),
                //         //         colorChooserDialog.Rgba.Green.ToString(),
                //         //         colorChooserDialog.Rgba.Blue.ToString()
                //         //     )
                //         // );
                //     }
                //     colorDialogInfo.Response = response;
                    returnValue = true;
                //}
                throw new NotImplementedException("GetColor");
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            colorDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a font descriptor.
        /// Static. Calls Avalonia.FontChooserDialog.
        /// </summary>
        /// <param name="fontDialogInfo">ref FontDialogInfo<Window, string, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetFont
        (
            ref FontDialogInfo<Window, string, string> fontDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            // Avalonia.FontChooserDialog fontChooserDialog = null;
            // string response = null;

            try
            {
            //     // fontResponse = null;
            //     fontChooserDialog =
            //         new FontChooserDialog
            //         (
            //             fontDialogInfo.Title,
            //             fontDialogInfo.Parent
            //         );
            //     fontChooserDialog.Modal = fontDialogInfo.Modal;

            //     response = (string)fontChooserDialog.Run();
            //     if (!string.IsNullOrWhiteSpace(response))
            //     {
            //         if (response == "Ok")
            //         {
            //             fontDialogInfo.FontDescription = fontChooserDialog.FontDesc;
            //         }
            //         fontDialogInfo.Response = response;
                     returnValue = true;
            //     }
                throw new NotImplementedException("GetFont");
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            fontDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Perform input of connection string and provider name.
        /// Uses MS Data Connections Dialog.
        /// Note: relies on MS-LPL license and code from http://archive.msdn.microsoft.com/Connection
        /// </summary>
        /// <param name="connectionString">ref string</param>
        /// <param name="providerName">ref string</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetDataConnection
        (
            ref string connectionString,
            ref string providerName,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            //DataConnectionDialog dataConnectionDialog = default(DataConnectionDialog);
            //DataConnectionConfiguration dataConnectionConfiguration = default(DataConnectionConfiguration);

            try
            {
                //dataConnectionDialog = new DataConnectionDialog();

                //DataSource.AddStandardDataSources(dataConnectionDialog);

                //dataConnectionDialog.SelectedDataSource = DataSource.SqlDataSource;
                //dataConnectionDialog.SelectedDataProvider = DataProvider.SqlDataProvider;//TODO:use?

                //dataConnectionConfiguration = new DataConnectionConfiguration(null);
                //dataConnectionConfiguration.LoadConfiguration(dataConnectionDialog);

                //(don't) set to current connection string, because it overwrites previous settings, requiring user to click Refresh in Data CConnection Dialog.
                //dataConnectionDialog.ConnectionString = connectionString;

                if (true/*DataConnectionDialog.Show(dataConnectionDialog) == DialogResult.OK*/)
                {
                    ////extract connection string
                    //connectionString = dataConnectionDialog.ConnectionString;
                    //providerName = dataConnectionDialog.SelectedDataProvider.ViewName;

                    ////writes provider selection to xml file
                    //dataConnectionConfiguration.SaveConfiguration(dataConnectionDialog);

                    ////save these too
                    //dataConnectionConfiguration.SaveSelectedProvider(dataConnectionDialog.SelectedDataProvider.ToString());
                    //dataConnectionConfiguration.SaveSelectedSource(dataConnectionDialog.SelectedDataSource.ToString());

                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
        #endregion Methods
    }
}
